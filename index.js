/*
	GitBash:
		npm init -y
		npm i express
		npm i mongoose
		touch .gitignore >> content: node_modules
*/
/*
	// models folder >> task.js
	// contollers folder >> taskController.js
	// route folder >> taskRoute.js
*/

//Setup dependencies/modules
const express = require("express");
const mongoose = require("mongoose");

// requiring an exported literal, function, or object
const taskRoute = require("./routes/taskRoute.js");

//Server Setup
const app = express();
const port = 3001;
//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://admin:admin@b218-to-do.nxzcomf.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);



//localhost:3001/tasks/ + add anoter endpoints
//localhost:3001/tasks/addNewTask

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Server running at port ${port}`));



